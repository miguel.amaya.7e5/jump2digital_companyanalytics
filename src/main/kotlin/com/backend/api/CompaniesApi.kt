package com.backend.api

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import com.backend.models.Company

/**
 * Esta clase se conecta a una api para leer un fichero json
 */

class CompaniesApi(private val client: HttpClient = defaultClient) {
    suspend fun getCompaniesListByJson(): MutableList<Company>? {
        val apiRoute = "https://challenges-asset-files.s3.us-east-2.amazonaws.com/jobMadrid/companies.json"
        return client.get(apiRoute).body()
    }
}