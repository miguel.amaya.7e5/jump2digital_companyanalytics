package com.backend.api

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json

/**
 * Instancia del cliente por defecto que se utilizará en todos los casos para hacer peticiones a apis
 * Si se necesitase crear otro cliente con otros parametros su instancia se encontraria aqui
 */
val defaultClient = HttpClient(CIO){
    install(ContentNegotiation) {
        json(Json {
            ignoreUnknownKeys = true
        })
    }
}