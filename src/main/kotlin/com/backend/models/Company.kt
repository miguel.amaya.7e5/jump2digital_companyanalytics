package com.backend.models

import kotlinx.serialization.Serializable

@Serializable
data class Company(
    val id: String,
    var website: String?,
    var name: String?,
    var founded: Int?,
    var size: String?,
    var locality: String?,
    var region: String?,
    var country: String?,
    var industry: String?,
    var linkedin_url: String?) {
}