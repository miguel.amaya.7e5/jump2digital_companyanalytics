package com.backend

import com.backend.api.CompaniesApi
import com.backend.data.CompanyDAO
import com.backend.data.DataBase
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import com.backend.plugins.*
import com.backend.storage.CompanyStorage
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.processNextEventInCurrentThread
import java.util.Scanner

/**
 * La aplicacion hace lo siguiente:
 *
 * 1: leer los datos de un archivo json, en este caso el json esta en una api
 * 2: introducir los datos obtenidos del json en una base de datos
 * 3: responder a las rutas con el resultado de busquedas en la base de datos
 *
 */

suspend fun main() {
    Application().start()
}

/**
 * Contiene las instancias y las inicializaciones de la app
 */
@OptIn(InternalCoroutinesApi::class)
class Application {
    private val dataBase = DataBase()
    private val companyDAO = CompanyDAO(dataBase)
    private val companyStorage = CompanyStorage()
    private val companiesApi = CompaniesApi()

    /**
     * Se conecta a la base de datos, carga los datos y inicia el servidor
     */
    suspend fun start() {
        dataBase.connect().use {
            initialiceData()
            embeddedServer(Netty, port = 8080, host = "0.0.0.0") {
                configureSerialization(companyDAO)
                configureRouting(companyDAO)
            }.start(wait = true)
        }
    }

    /**
     * Comprueba que se puedan cargar los datos antes de borrar los antiguos
     */
    private suspend fun initialiceData() {
        try {
            if (companiesApi.getCompaniesListByJson() != null)
                processNextEventInCurrentThread()
        } catch (e: Exception) {
            e.printStackTrace()
            return
        }
        insertCompanyData()
    }

    /**
     * Borra los datos antiguos de la BD si los hubiese y inserta los nuevos en la base de datos
     */
    private suspend fun insertCompanyData() {
        if (!companyDAO.checkDataBaseIsEmpty())
            companyDAO.deleteCompanyTable()
        companyDAO.createTableIfNotExists()
        companyStorage.companyList = companiesApi.getCompaniesListByJson()!!
        companyDAO.insertData(companyStorage.companyList)
    }
}