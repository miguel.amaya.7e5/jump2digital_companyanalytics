package com.backend.routes

import com.backend.data.CompanyDAO
import com.backend.models.Company
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.companyRoutes(companyDAO: CompanyDAO) {
    var respondList: List<Company>?

    route("/companies") {

        get("all") {
            if (companyDAO.checkDataBaseIsEmpty())
                call.respondText("No companies found", status = HttpStatusCode.OK)
            else {
                respondList = companyDAO.getAllCompanies()
                call.respond(respondList!!)
            }
        }
        get("order_by_size") {
            respondList = companyDAO.getCompaniesBySize() ?: return@get call.respondText("Not companies found", status = HttpStatusCode.NotFound)
            if (respondList != null ) call.respond(respondList!!)
            else call.respondText("Nou found", status = HttpStatusCode.Conflict)
        }

        get("order_by_founded") {
            respondList = companyDAO.getCompaniesByFounded() ?: return@get call.respondText("Not companies found", status = HttpStatusCode.NotFound)
            if (respondList != null ) call.respond(respondList!!)
            else call.respondText("Nou found", status = HttpStatusCode.Conflict)
        }
        /**
         * Ejecuta 3 consultas que devuelven una lista de Pair<String,Int> cada una
         * Devuelve una matriz con las 3 listas de Pairs
         */
        get("tarea4_solution") {
            val industryCompaniesCount = companyDAO.getCompaniesJoinedByIndustry()
            val sizeCompaniesCount = companyDAO.getCompaniesJoinedBySize()
            val foundedCompaniesCount = companyDAO.getCompaniesJoinedByFounded()
            val respond = mutableListOf(industryCompaniesCount,sizeCompaniesCount,foundedCompaniesCount)
            call.respond(respond)
        }
    }
}