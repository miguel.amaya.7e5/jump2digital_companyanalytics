package com.backend.data

import java.sql.Connection
import java.sql.DriverManager

/**
 * Esta es la instancia de la base de datos y donde se crea la conexión
 */

const val databaseFile = "sample.db"

class DataBase {
    var connection: Connection? = null

    fun connect(): Connection {
        connection = DriverManager.getConnection("jdbc:sqlite:$databaseFile")
        return connection!!
    }
}