package com.backend.data

import com.backend.models.Company
import java.sql.Connection
import java.sql.PreparedStatement

/**
 * Esta clase inserta, extrae y modifica informacion de la base de datos
 */
class CompanyDAO(private val dataBase: DataBase) {
    val connection: Connection get() = dataBase.connect()
    private var createStatement = connection.createStatement()
    private var query = ""

    /**
     * Para crear la tabla por primera vez
     */
    fun createTableIfNotExists() {
        val query = "CREATE TABLE IF NOT EXISTS companies" +
                "(id VARCHAR PRIMARY KEY," +
                "website VARCHAR NULL," +
                "named VARCHAR NULL," +
                "founded INTEGER NULL," +
                "sized VARCHAR NULL," +
                "locality VARCHAR NULL," +
                "region VARCHAR NULL," +
                "country VARCHAR NULL," +
                "industry VARCHAR NULL," +
                "linkedin_url VARCHAR NULL)"
        val createStatement = connection.prepareStatement(query)
        createStatement.execute()
    }

    /**
     * Insertar los datos obtenidos del json en la base de datos
     */
    fun insertData(companiesList: MutableList<Company>) {
        query = "INSERT INTO companies VALUES (?,?,?,?,?,?,?,?,?,?)"
        var statment: PreparedStatement
        var founded: Int

        companiesList.forEach {
            founded =
                if (it.founded == null) 0
                else it.founded!!
            statment = connection.prepareStatement(query).apply {
                setString(1, it.id)
                setString(2, it.website)
                setString(3, it.name)
                setInt(4, founded)
                setString(5, it.size)
                setString(6, it.locality)
                setString(7, it.region)
                setString(8, it.country)
                setString(9, it.industry)
                setString(10, it.linkedin_url)
                execute()
            }
        }
    }
    /**
     * Para comprobar que los datos ya estan introducidos
     */
    fun checkDataBaseIsEmpty(): Boolean {
        query = "SELECT id FROM companies"
        val result = createStatement.executeQuery(query)

        val checkData = mutableListOf<String>()
        while (result.next())
            checkData.add(result.getString("id"))
        return checkData.isEmpty()
    }

    fun getAllCompanies(): List<Company>? {
        query = "SELECT * FROM companies"
        return executeCompanyQuery(query)
    }
    fun getCompaniesBySize(): List<Company>? {
        query = "SELECT * FROM companies ORDER BY sized"
        return executeCompanyQuery(query)
    }
    fun getCompaniesByFounded(): List<Company>? {
        query = "SELECT * FROM companies ORDER BY founded"
        return executeCompanyQuery(query)
    }
    fun deleteCompanyTable() {
        query = "drop table companies"
        val createStatement = connection.prepareStatement(query)
        createStatement.execute()
    }

    fun getCompaniesJoinedByIndustry(): MutableList<Pair<String,Int>> {
        query = "SELECT industry, count(*) as counter FROM companies GROUP BY industry"
            val result = createStatement.executeQuery(query)
            val companiesList = mutableListOf<Pair<String,Int>>()
            while (result.next()) {
                companiesList.add(
                    Pair(result.getString("industry"), result.getInt("counter")))
            }
        return companiesList
    }
    fun getCompaniesJoinedBySize(): MutableList<Pair<String,Int>> {
        query = "SELECT sized, count(*) as counter FROM companies GROUP BY sized"
        val result = createStatement.executeQuery(query)
        val companiesList = mutableListOf<Pair<String,Int>>()
        while (result.next()) {
            companiesList.add(
                Pair(result.getString("sized"), result.getInt("counter")))
        }
        return companiesList
    }
    fun getCompaniesJoinedByFounded(): MutableList<Pair<String,Int>> {
        query = "SELECT founded, count(*) as counter FROM companies GROUP BY founded"
        val result = createStatement.executeQuery(query)
        val companiesList = mutableListOf<Pair<String,Int>>()
        while (result.next()) {
            companiesList.add(
                Pair(result.getString("founded"), result.getInt("counter")))
        }
        return companiesList
    }

    /**
     * Ejecuta cualquier consulta que empiece por SELECT * FROM companies
     */
    private fun executeCompanyQuery(query: String): List<Company>? {
        if ("SELECT * FROM companies" in query) {
            val result = createStatement.executeQuery(query)
            val companiesList = mutableListOf<Company>()
            while (result.next()) {
                companiesList.add( Company(
                    result.getString("id"),
                    result.getString("website"),
                    result.getString("named"),
                    result.getInt("founded"),
                    result.getString("sized"),
                    result.getString("locality"),
                    result.getString("region"),
                    result.getString("country"),
                    result.getString("industry"),
                    result.getString("linkedin_url"))
                )
            }
            companiesList.forEach { if (it.founded == 0) it.founded = null }
            return companiesList
        } else return null
    }
}

