package com.backend.storage

import com.backend.models.Company

/**
 * Para guardar los datos del json en una lista
 */

class CompanyStorage {
    var companyList: MutableList<Company> = mutableListOf()
}