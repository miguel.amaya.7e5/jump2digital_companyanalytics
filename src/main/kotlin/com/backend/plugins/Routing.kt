package com.backend.plugins

import com.backend.data.CompanyDAO
import com.backend.data.DataBase
import com.backend.routes.companyRoutes
import com.backend.storage.StorageIntances
import io.ktor.server.routing.*
import io.ktor.server.application.*

fun Application.configureRouting(companyDAO: CompanyDAO) {
    routing {
        companyRoutes(companyDAO)
    }
}
