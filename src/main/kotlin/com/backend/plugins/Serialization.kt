package com.backend.plugins

import com.backend.data.CompanyDAO
import com.backend.routes.companyRoutes
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.application.*
import io.ktor.server.routing.*

fun Application.configureSerialization(companyDAO: CompanyDAO) {
    install(ContentNegotiation) {
        json()
    }
    routing {
        companyRoutes(companyDAO)
    }
}
